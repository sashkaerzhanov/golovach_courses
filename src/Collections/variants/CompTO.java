package Collections.variants;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alexander Erzhanov
 * @since 11.07.2017
 */
public class CompTO {
    public static void main(String[] args) {
        String str0 = "A";
        String str1 = "B";
        System.out.println(str0.compareTo(str1));
        System.out.println(str1.compareTo(str0));
        System.out.println(str0.compareTo(str0));

        set(new ArrayList<Integer>(),new ArrayList<Integer>());

    }

    public static <T> int lenth0 (List<T> list){
        return list.size();                     //можно использовать в дальнейшем
    }

    public static  int lenth1 (List<?> list){
        return list.size();                   //если в дальнейшем не нужно использовать
    }

    public static <T> T first0 (List<T> list){
        return list.get(0);                   // в качестве типа функции используем Т
    }

//    public static ? first1 (List<?> list){
//        return list.get(0);                   //в тип функции нельзя написать "?"
//    }


    public static <T> void set (List<T> list0, List<T> list1){
        list0.set(0,list1.get(0));
    }
}

