package Collections.variants;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alexander Erzhanov
 * @since 11.07.2017
 */
public class GenericXXXVariant {
    public static void main(String[] args) {
        List<String> strArr = new ArrayList<>();
        List <? extends Object> objArr = strArr;
    //    objArr.set(0, new Integer(0));
        String str = strArr.get(0);
    }
}
