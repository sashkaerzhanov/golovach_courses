package Collections.variants;

/**
 * @author Alexander Erzhanov
 * @since 11.07.2017
 */
public class ArrayCoVariant {
    public static void main(String[] args) {
        String [] strArr = {"A","B","B"};
        Object [] objArr = strArr;
        objArr[0] = new int[]{100,2000};
        String str = strArr[0];

    }
}
