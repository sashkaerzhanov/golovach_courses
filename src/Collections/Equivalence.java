package Collections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 * @author Alexander Erzhanov
 * @since 05.07.2017
 */
public class Equivalence {
    public static void main(String[] args) {

        List<String> list0 = new ArrayList<>();
        list0.add("A");
        list0.add("B");

        List <String> list1 = new ArrayList<>();
        list1.add("B");
        list1.add("A");
        System.out.println(list0.equals(list1));

        Collection<String> set0 = new HashSet<>();
        set0.add("A");
        set0.add("B");

        Collection <String> set1 = new HashSet<>();
        set1.add("B");
        set1.add("A");
        System.out.println(set0.equals(set1));
    }
}
