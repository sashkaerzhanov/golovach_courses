package Collections;

import java.util.HashSet;
import java.util.NavigableMap;
import java.util.TreeMap;

/**
 * @author Alexander Erzhanov
 * @since 13.07.2017
 */
public class NavigMap {
    public static void main(String[] args) {
        NavigableMap<String,Integer>  map = new TreeMap<>();
        map.put("A", 0);
        map.put("B", 1);
        map.put("c", 0);
        map.put("B", 3);
        map.put(map.lastKey(), map.get(map.lastKey()+1)); // В ключ С вносим значение - елемент которого нет, то-есть null
        System.out.println(map);
        System.out.println(new HashSet<>(map.keySet()).size());
        System.out.println(new HashSet<>(map.values()).size());

    }
}
