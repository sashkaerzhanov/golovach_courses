package Collections;

import java.util.Arrays;

/**
 * @author Alexander Erzhanov
 * @since 11.07.2017
 */
public class GenericHolderTest {
    public static void main(String[] args) {
        //set String
        GenericHolder<String> strhold = new GenericHolder<>();
        strhold.setData("Hello");
        //getString
        String str = strhold.getData();
        System.out.println(str);

        //setInt
        GenericHolder<int[]> inthold = new GenericHolder<>();
        inthold.setData(new int[]{10,20,35});
        //getInt
        int [] arr = inthold.getData();
        System.out.println(Arrays.toString(arr));

    }
}
