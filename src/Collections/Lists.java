package Collections;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alexander Erzhanov
 * @since 02.07.2017
 */
public class Lists {

    public static void main(String[] args) {

        List<String> list = new ArrayList<>();
        list.add("a");
        list.add("b");
        list.add("c");
        System.out.println(list);
        System.out.println(list.get(2));
        list.set(2, "D");  //меняет уже существующий елемент
        System.out.println(list);

        // особенности коллекции
        list.add(1,"X");//раздвиuает текущие елементы и вставляет новый
        System.out.println(list);

        list.remove(1);     //убирает елемент
        System.out.println(list);






    }

}
