package Collections;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static java.util.Arrays.asList;

/**
 * @author Alexander Erzhanov
 * @since 05.07.2017
 */
public class PushIteratorTest {
    public static void main(String[] args) {
//        List<Integer> list =list(1,2,3);
//        Iterator<Integer> iter = new Iterator<Integer>() { //nested class
//            private Random rd = new Random();
//
//            public boolean hasNext() {
//                return true;
//            }
//
//            public Integer next() {
//                return rd.nextInt();
//            }
//        };
        Iterator<Integer> iter = asList(1,2,3).iterator();
        iter = new PushIterator(0, iter);
        while (iter.hasNext()){
            System.out.println(iter.next());

        }

    }
    public static class PushIterator implements Iterator<Integer>{
        private int value;
        private Iterator<Integer> iter;
        private boolean valueUsed = true;

        public PushIterator(int value, Iterator<Integer> iter) {
            this.value = value;
            this.iter = iter;
        }

        @Override
        public boolean hasNext() {
            return valueUsed ? true: iter.hasNext();
        }

        @Override
        public Integer next() {
            if (valueUsed) {
               valueUsed = false;
               return value;
            }

            else {
                return iter.next();

            }



        }
    }










/*
        Factory method : метод заменяющий конструктор, для добавления
        элементов в list
 */

    public static <T> List<T> list(T...args) {
        List<T> result = new ArrayList<>();
        for (T elem : args) {
            list().add(elem);
        }
        return result;
    }
}
