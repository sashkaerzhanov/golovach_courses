package Collections;

import java.util.Comparator;

/**
 * @author Alexander Erzhanov
 * @since 12.07.2017
 */
public class LenthComparator implements Comparator <String> {

    @Override
    public int compare(String str1, String str2) {
        int result = str1.length()-str2.length();
        if(result ==0){
            return str1.compareTo(str2);
        } else {
            return result;
        }
    }
}
