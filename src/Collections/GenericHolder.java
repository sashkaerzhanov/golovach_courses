package Collections;

/**
 * @author Alexander Erzhanov
 * @since 11.07.2017
 */
public class GenericHolder<T> {
    private T data;

    public T getData(){
        return data;

    }

    public void setData(T data){
        this.data = data;
    }

}
