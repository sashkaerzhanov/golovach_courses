package Collections;

import java.util.Comparator;

/**
 * @author Alexander Erzhanov
 * @since 12.07.2017
 */
public class PersonComparator implements Comparator <Treeeset.Person> {

    @Override
    public int compare(Treeeset.Person p1, Treeeset.Person p2) {
        int res = p1.getName().compareTo(p2.getName());
        if (res == 0) {
            return p1.getAge()-p2.getAge();
        }
        else {
            return res;
        }

    }
}
