package Collections;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Alexander Erzhanov
 * @since 13.07.2017
 */
public class Maps {
    public static void main(String[] args) {
        Map <String, Map<String,String>> map = new HashMap<>();
        map.put("A", newMap("A","A"));
        map.put("B", newMap("A","B"));
        map.put("D", newMap("B","C"));
        map.put("C", newMap("C","D"));
        map.put("A", newMap("D","C")); //записываеться поверх первого
        System.out.println(map.get("A").get("D"));
    }

    public static <K,V> Map<K,V> newMap(K key, V value){
        HashMap<K,V> map = new HashMap<>();
        map.put(key,value);
        return map;
    }
}
