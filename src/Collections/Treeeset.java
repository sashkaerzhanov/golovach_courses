package Collections;

import java.util.NavigableSet;
import java.util.TreeSet;

/**
 * @author Alexander Erzhanov
 * @since 12.07.2017
 */
public class Treeeset {
    public static void main(String[] args) {
//        NavigableSet<String> tree = new TreeSet<>();
//        tree.add("A");
//        tree.add("C");
//        tree.add("R");
//        tree.add("B");
//        tree.add("D");
//        System.out.println(tree);
//        System.out.println(tree.subSet("B",true,"D",true));
//        System.out.println(tree.subSet("B",false,"D",true));
//        System.out.println(tree.pollFirst());
//        System.out.println(tree.pollFirst());
//        System.out.println(tree);

//                    NavigableSet<String> tree = new TreeSet<>(new LenthComparator());
//                    tree.add("A");
//                    tree.add("bbbb");
//                    tree.add("c");
//                    tree.add("FFFFFFFFFF");
//                    tree.add("DDD");
//                    tree.add("MMMMMMMMMMMMMM");
//                    tree.add("OOO");
//                    tree.add("PPPPPP");
//
//                    System.out.println(tree);



        NavigableSet<Person> tree1 = new TreeSet<>(new PersonComparator());
        tree1.add(new Person("alex",21));
        tree1.add(new Person("max",25));
        tree1.add(new Person("vlad",23));
        tree1.add(new Person("dimon",18));
        tree1.add(new Person("anna",30));
        tree1.add(new Person("dimon",27));
        System.out.println(tree1);
 //       System.out.println(tree1.subSet(20,true , 25,true));
    }

    public static class Person implements Comparable<Person>{
        private String Name;
        private int age;

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        public Person(String name, int age) {
            Name = name;
            this.age = age;
        }

        @Override
        public int compareTo(Person that) {
            return this.age - that.age;
        }

        @Override
        public String toString() {
            return "Person{" +
                    "Name='" + Name + '\'' +
                    ", age=" + age +
                    '}';
        }
    }
}
