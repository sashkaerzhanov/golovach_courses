package Collections;

import java.util.Collection;
import java.util.TreeSet;

/**
 * @author Alexander Erzhanov
 * @since 27.06.2017
 */

public class Colection {
    public static void main(String[] args) {
    //    Collection <String> A = new ArrayList <> ();
     //   Collection <String> A = new HashSet<>();
        Collection <String> A = new TreeSet<>();
            A.add ("a");
            A.add ("b");
            A.add ("c");

    //    Collection <String> B = new ArrayList <> ();
    //    Collection <String> B = new HashSet<> ();
        Collection <String> B = new TreeSet<>();
            B.add ("b");
            B.add ("c");
            B.add ("d");

     //   A.addAll(B);
        A.retainAll(B);
        System.out.println(A);
        System.out.println(A.size());
        System.out.println();

    }

}
