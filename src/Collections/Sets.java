package Collections;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Alexander Erzhanov
 * @since 03.07.2017
 */
public class Sets {
    public static void main(String[] args) {
//        Set<String> set = new HashSet<>();
//        set.add("AAA");
//        set.add("BBB");
//        set.add("CCC");
//        set.add("AAA");
//        System.out.println(set);
//        System.out.println(set.contains("XXX"));
//        System.out.println(set.contains("BBB"));
//        set.remove("BBB");
        Set<Employee> set = new HashSet<>();
        set.add(new Employee("Alex", 32));
        set.add(new Employee("Alex", 32));
        set.add(new Employee("Vlad", 25));
        set.add(new Employee("Yarik", 21));
        System.out.println(set);
        System.out.println(set.contains(new Employee("Alex", 32)));

//        Iterator<String> iter = set.iterator();
//        while (iter.hasNext()) {
//            System.out.println(iter.next());
//        }
//
//        for (Iterator<String> iter = set.iterator();iter.hasNext(); )
//            System.out.println(iter.next());

//            for (String elem :set) {
//                System.out.println(elem);
//            }

    }

    public static class Employee {
        private String name;
        private int age;

        private Employee(String name, int age) {
            this.name = name;
            this.age = age;
        }

        @Override
        public boolean equals(Object o) {
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            Employee that = (Employee) o;

            if (this.age != that.age) {
                return false;
            }
            if( this.name == null){
                return that.name == null;
            } else{
   //             return this.name.equals(that.name);

              char[]ch0 = this.name.toCharArray();
              char [] ch1 = this.name.toCharArray();
              Arrays.sort(ch0);
              Arrays.sort(ch1);
              return Arrays.equals(ch0, ch1);

            }
//            return Objects.equals(this.age, that.age)  //при сравнении интов будут созданы Integer!!
//                    && Objects.equals(this.name,that.name);
        }

        @Override
        public int hashCode() {
            int result = name != null ? name.hashCode() : 0;
            result = 31 * result + age;
            return result;
        }

        @Override
        public String toString() {
            return getClass().getSimpleName() + "{" +
                    "name='" + name + '\'' +
                    ", age=" + age +
                    '}';
        }

    }


}
